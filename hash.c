#include "py.h"
HASH_TABLE gHashTable[HASH_SIZE] = {{ 0 }};

unsigned long
hash_djb2(
	unsigned char *Str
	)
{
	unsigned long hash = 5381;
	int c;

	while (*Str)
	{
		c = *Str;
		Str++;
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
	}

	return hash;
}

unsigned long
h1(
	unsigned char *Str
	)
{
	unsigned long hash = 7;
	int c;
	while (*Str)
	{
		c = *Str;
		Str++;
		hash = 31 * hash * hash + 101 * hash + c + 17;
	}
	return hash;
}

int
quadratic_probing_insert(
	PHASH_TABLE Hashtable,
	int Key
	)
{
	/* Hashtable[] is an integer hash table; empty[] is another array which indicates whether the key space is occupied;
	If an empty key space is found, the function returns the index of the bucket where the key is inserted, otherwise it
	returns (-1) if no empty key space is found */

	int i, index;
	for (i = 0; i < HASH_SIZE; i++) 
	{
		index = (Key + i*i) % HASH_SIZE;
		if (!Hashtable[index].used)
		{
			Hashtable[index].key = Key;
			Hashtable[index].used = 1;
			return index;
		}
		else if (Hashtable[index].key == Key)
		{
			return index;
		}
	}
	return -1;
}

int
quadratic_probing_search(
	PHASH_TABLE Hashtable,
	int Key
	)
{
	/* If the key is found in the hash table, the function returns the index of the Hashtable where the key is inserted, otherwise it
	returns (-1) if the key is not found */

	int i, index;

	for (i = 0; i < HASH_SIZE; i++) 
	{
		index = (Key + i*i) % HASH_SIZE;
		if (!Hashtable[index].used)
		{
			break;
		}
		else if (Hashtable[index].key == Key)
		{
			return index;
		}
	}
	return -1;
}