#include "py.h"

void
printList(
	PLIST List
	)
{
	PLIST element = NULL;
	printf("< ");
	if (List == NULL)
	{
		printf("NULL");
	}
	for (element = List; element != NULL; element = element->next)
	{
		printNode(&element->value, NULL);
		printf(" ");
	}
	printf(">");
}

PLIST
consListNode(
	OBJECT Value
	)
{
	PLIST node = NULL;
	node = (PLIST)malloc(sizeof(*node));
	node->value = Value;
	node->next = NULL;
	return node;
}

PLIST
listSlice(
	PLIST List,
	int Index1,
	int Index2
	)
{
	PLIST destination = NULL;
	PLIST result = NULL;
	PLIST prev = NULL;
	PLIST iterator = NULL;
	int length = 0;
	if (!List)
	{
		return NULL;
	}
	
	if (Index1 >= Index2)
	{
		return NULL;
	}
	
	length = Index2 - Index1;
	
	while (Index1 && List)
	{
		List = List->next;
		Index1--;
	}
	
	if (!List)
	{
		return NULL;
	}
	
	destination = consListNode(List->value);
	if (!destination)
	{
		return NULL;
	}
	result = destination;
	prev = destination;
	length--;
	
	for (iterator = List->next; iterator && length; iterator = iterator->next)
	{
		destination = consListNode(iterator->value);
		if (!destination)
		{
			return NULL;
		}
		prev->next = destination;
		prev = destination;
		length--;
	}
	return result;
}

PLIST
listDup(
	PLIST List1,
	PLIST List2
	)
{
	PLIST destination = NULL;
	PLIST result = NULL;
	PLIST prev = NULL;
	PLIST iterator = NULL;
	
	if (!List1)
	{
		if (List2)
		{
			List1 = List2;
			List2 = NULL;
		}
		else
		{
			return NULL;
		}
	}
	
	destination = consListNode(List1->value);
	if (!destination)
	{
		return NULL;
	}
	result = destination;
	prev = destination;
	
	
	for (iterator = List1->next; iterator != NULL; iterator=iterator->next)
	{
		destination = consListNode(iterator->value);
		if (!destination)
		{
			return NULL;
		}
		prev->next = destination;
		prev = destination;
	}
	for (iterator = List2; iterator != NULL; iterator=iterator->next)
	{
		destination = consListNode(iterator->value);
		if (!destination)
		{
			return NULL;
		}
		prev->next = destination;
		prev = destination;
	}
	
	return result;
}

PLIST
append(
	PLIST List1,
	PLIST List2
	)
{
	PLIST element = NULL;
	PLIST prev = NULL;
	
	DEBUG_MSG("append was called\n");
	for (element = List1, prev = List1; element != NULL; prev = element,element = element->next);
	prev->next = List2;
	return List1;
}

PLIST
createNode(
	)
{
	PLIST element = NULL;
	element = (PLIST)malloc(sizeof(*element));
	if (!element)
	{
		return NULL;
	}
	memset(element, 0, sizeof(*element));
	return element;
}

POBJECT
setListElement(
	PLIST *List,
	int Index,
	POBJECT Value
	)
{
	PLIST element = NULL;
	PLIST prev = NULL;
	int index = 0;
	int wasFound = 0;
	if (!List)
	{
		return NULL;
	}
	if (index < 0)
	{
		return NULL;
	}
	for (element = *List; element != NULL; prev = element, element = element->next)
	{
		if (Index == index)
		{
			memcpy(&element->value, Value, sizeof(*Value));
			wasFound = 1;
			break;
		}
		index++;
	}
	
	if (!wasFound)
	{
		while (index <= Index)
		{
			element = createNode();
			if (!element)
			{
				printf("allocating node failed");
				return NULL;
			}
			if (!*List)
			{
				*List = element;
			}
			if (prev)
			{
				prev->next = element;
			}
			prev = element;
			if (index == Index)
			{
				if (Value)
				{
					memcpy(&element->value, Value, sizeof(*Value));	
				}
			}
			index++;
		}
	}
	
	return &element->value;
}

POBJECT
getListElement(
	PLIST List,
	int Index
	)
{
	PLIST element = NULL;
	int index = 0;
	
	for (element = List; element != NULL; element = element->next)
	{
		if (index == Index)
		{
			return &element->value;
		}
		index++;
	}
	return NULL;
}

PLIST
cons_list(
	PLIST List,
	int Element
	)
{
	PLIST element = NULL;
	
	element = createNode();
	if (!element)
	{
		printf("Error creating node!\n");
		return NULL;
	}
	element->value.nodeInfo.ival = Element;
	element->value.type = NUMB;
	element->next = List;
	return element;
}

int
len(
	PLIST List
	)
{
	int count = 0;
	while (List)
	{
		List = List->next;
		count++;
	}
	return count;
}