#include "py.h"

#define EXCEPTION printf("RUNTIME EXCEPTION at %s %d\n", __FILE__, __LINE__)

#define STATUS_ERROR -1
#define STATUS_SUCCESS 0
#define IS_SUCCESS(status) ((status) >= 0)
#define STATUS_CLEANUP(status) do {if(!IS_SUCCESS(status)){EXCEPTION; goto cleanup;}} while(0)

void
freeAndNull(void* Object)
{
	if (!Object)
	{
		return;
	}
	if (*(char**)Object)
	{
		free(*(char**)Object);
		*(char**)Object = NULL;
	}
}

PNODE
cons_node(
	int Type,
	NODE_INFO NodeInfo,
	PNODE Left,
	PNODE Right
	)
{
	PNODE node = NULL;
	node = malloc(sizeof(*node));
	if (!node)
	{
		printf ("malloc failed!\n");
		return NULL;
	}
	node->obj.type = Type;
	node->obj.nodeInfo = NodeInfo;
	node->left = Left;
	node->right = Right;
	return node;
}

char*
getType(
	int Type
	)
{
	if (Type == NUMB)
	{
		return "NUMB";
	}
	else if (Type == LIST_ELEMENT)
	{
		return "element";
	}
	else if (Type == LIST_SLICE)
	{
		return "slice";
	}
	else if (Type == LIST_TYPE)
	{
		return "list";
	}
	else if (Type == VARIABLE)
	{
		return "var";
	}
	else if (Type == NOT_ASSIGNED_TYPE)
	{
		return "undef";
	}
	else
	{
		return "unknown";
	}
}

void
freeAst(
	PNODE Node
	)
{
	if (!Node)
	{
		return;
	}
	freeAst(Node->left);
	freeAst(Node->right);
	if (Node->obj.type == LIST_TYPE)
	{
		free(Node->obj.nodeInfo.lval);
	}
	free(Node);
}

STATUS_TYPE
parseAst(
	PNODE Ast,
	POBJECT Result,
	POBJECT *FieldReference
	)
{
	POBJECT variableObjectReference = NULL;
	STATUS_TYPE status = STATUS_ERROR;
	OBJECT left = {0};
	OBJECT right = {0};
	
	if (!Ast)
	{
		goto cleanup;
	}
	
	if (!Result)
	{
		goto cleanup;
	}
	memset(Result, 0, sizeof(*Result));
	
	DEBUG_MSG("Ast node: %s\n", getType(Ast->obj.type));
	
	if (Ast->obj.type == IF)
	{
		status = parseAst(Ast->left, &left, NULL);
		STATUS_CLEANUP(status);
		
		if (left.nodeInfo.ival)
		{
			status = parseAst(Ast->right, &right, NULL);
			STATUS_CLEANUP(status);
		}
		
		Result->type = MISSING_TYPE;
		status = STATUS_SUCCESS;
		goto cleanup;
	}
	else if (Ast->obj.type == WHILE)
	{
		status = parseAst(Ast->left, &left, NULL);
		STATUS_CLEANUP(status);
		
		while (left.nodeInfo.ival)
		{
			status = parseAst(Ast->right, &right, NULL);
			STATUS_CLEANUP(status);
			
			status = parseAst(Ast->left, &left, NULL);
			STATUS_CLEANUP(status);
		}
		
		Result->type = MISSING_TYPE;
		status = STATUS_SUCCESS;
		goto cleanup;
	}
	else if (Ast->obj.type == NUMB)
	{
		Result->nodeInfo.ival = Ast->obj.nodeInfo.ival;
		Result->type = NUMB;
		
		status = STATUS_SUCCESS;
		goto cleanup;
	}
	else if (Ast->obj.type == LIST_TYPE)
	{
		Result->nodeInfo.lval =  Ast->obj.nodeInfo.lval;
		Result->type = LIST_TYPE;
		
		status = STATUS_SUCCESS;
		goto cleanup;
	}
	else if (Ast->obj.type == VARIABLE)
	{
		unsigned long key = Ast->obj.nodeInfo.ival;
		int index = quadratic_probing_search(gHashTable, key);
		if (index == -1)
		{
			index = quadratic_probing_insert(gHashTable, key);
			if (index == -1)
			{
				printf("INTERNAL HASH ERROR!\n");
				
				status = STATUS_ERROR;
				goto cleanup;
			}
			// return the pointer to the object in the hashtable
			variableObjectReference = &gHashTable[index].value;
		}
		else
		{
			variableObjectReference = &gHashTable[index].value;
		}
		
		status = STATUS_SUCCESS;
		goto cleanup;
	}
	else if (Ast->obj.type == '=')
	{
		int index;
		int type;
		unsigned long key = 0;
		
		if (Ast->left->obj.type == VARIABLE
			|| Ast->left->obj.type == LIST_ELEMENT)
		{
			POBJECT varElement = NULL;
			
			status = parseAst(Ast->left, &left, &varElement);
			STATUS_CLEANUP(status);
			
			if (!varElement)
			{
				EXCEPTION;
				goto cleanup;
			}
			
			status = parseAst(Ast->right, &right, NULL);
			STATUS_CLEANUP(status);
			
			type = right.type;
			varElement->type = type;
			if (type == LIST_TYPE)
			{
				varElement->nodeInfo.lval = listDup(right.nodeInfo.lval, NULL);
			}
			else
			{
				varElement->nodeInfo.ival = right.nodeInfo.ival;
			}

			Result->nodeInfo = right.nodeInfo;
			Result->type = right.type;
			
			status = STATUS_SUCCESS;
			goto cleanup;
		}
		else
		{
			EXCEPTION;
			printNode(&Ast->left->obj, NULL);
			goto cleanup;
		}
	}
	else if (Ast->obj.type == LIST_ELEMENT)
	{
		int index = 0;
		POBJECT element = NULL;
		POBJECT varElement = NULL;
		
		status = parseAst(Ast->left, &left, &varElement);
		STATUS_CLEANUP(status);
		
		if (!varElement)
		{
			EXCEPTION;
			goto cleanup;
		}
		
		status = parseAst(Ast->right, &right, NULL);
		STATUS_CLEANUP(status);
		
		if (right.type != NUMB)
		{
			EXCEPTION;
			printf("list index not a number!");
			goto cleanup;
		}
		
		if (varElement->type != LIST_TYPE && varElement->type != NOT_ASSIGNED_TYPE)
		{
			EXCEPTION;
			printf("only lists can be indexed: type: %s\n", getType(varElement->type));
			goto cleanup;
		}
		
		index = right.nodeInfo.ival;
		element = getListElement(varElement->nodeInfo.lval, index);
		if (!element)
		{
			element = setListElement(&varElement->nodeInfo.lval, index, NULL);
			if (!element)
			{
				EXCEPTION;
				goto cleanup;
			}
			varElement->type = LIST_TYPE;
		}
		DEBUG_MSG("type: LIST_ELEMENT\n");
		
		//return the element itself, not just a copy
		//this is useful for the autovivification mechanism
		variableObjectReference = element;
		
		status = STATUS_SUCCESS;
		goto cleanup;
	}
	else if (Ast->obj.type == LIST_SLICE)
	{
		POBJECT element = NULL;
		POBJECT firstIndex = NULL;
		POBJECT secondIndex = NULL;
		PLIST resultingList = NULL;
		
		status = parseAst(Ast->left, &left, NULL);
		STATUS_CLEANUP(status);
		
		if (left.type != LIST_TYPE)
		{
			EXCEPTION;
			printf("variable is not a list!\n");
			goto cleanup;
		}
		
		status = parseAst(Ast->right, &right, NULL);
		STATUS_CLEANUP(status);
		
		if (right.type != LIST_TYPE)
		{
			EXCEPTION;
			printf("list slice not a list!\n");
			goto cleanup;
		}

		firstIndex = getListElement(right.nodeInfo.lval, 0);
		if (!firstIndex)
		{
			EXCEPTION;
			goto cleanup;
		}

		secondIndex = getListElement(right.nodeInfo.lval, 1);
		if (!secondIndex)
		{
			EXCEPTION;
			goto cleanup;
		}
		
		resultingList = listSlice(left.nodeInfo.lval, firstIndex->nodeInfo.ival, secondIndex->nodeInfo.ival);
		
		Result->type = LIST_TYPE;
		Result->nodeInfo.lval = resultingList;
		
		status = STATUS_SUCCESS;
		goto cleanup;
	}
	else if (Ast->obj.type == PRINT)
	{
		status = parseAst(Ast->left, &left, NULL);
		STATUS_CLEANUP(status);
		DEBUG_MSG("print\n");
		memcpy(Result, &left, sizeof(*Result));
		printf("%s", getType(Result->type));
		printf("-->");
		printNode(Result, NULL);
		printf("\n");
		
		status = STATUS_SUCCESS;
		goto cleanup;
	}
	
	status = parseAst(Ast->left, &left, NULL);
	STATUS_CLEANUP(status);
	
	status = parseAst(Ast->right, &right, NULL);
	STATUS_CLEANUP(status);

	if (Ast->obj.type == ';')
	{
		DEBUG_MSG("concat\n");
		memcpy(Result, &right, sizeof(*Result));
	}
	else if ((left.type == right.type) && (right.type == NUMB))
	{
		if (Ast->obj.type == '+')
		{	
			Result->nodeInfo.ival = left.nodeInfo.ival + right.nodeInfo.ival;
		}
		else if (Ast->obj.type == '-')
		{
			Result->nodeInfo.ival = left.nodeInfo.ival - right.nodeInfo.ival;
		}
		else if (Ast->obj.type == '*')
		{
			Result->nodeInfo.ival = left.nodeInfo.ival * right.nodeInfo.ival;
		}
		else if (Ast->obj.type == '/')
		{
			Result->nodeInfo.ival = left.nodeInfo.ival / right.nodeInfo.ival;
		}
		else if (Ast->obj.type == GE)
		{
			Result->nodeInfo.ival = left.nodeInfo.ival >= right.nodeInfo.ival;
		}
		else if (Ast->obj.type == LE)
		{
			Result->nodeInfo.ival = left.nodeInfo.ival <= right.nodeInfo.ival;
		}
		else if (Ast->obj.type == EQ)
		{
			Result->nodeInfo.ival = left.nodeInfo.ival == right.nodeInfo.ival;
		}
		else if (Ast->obj.type == NE)
		{
			Result->nodeInfo.ival = left.nodeInfo.ival != right.nodeInfo.ival;
		}
		else if (Ast->obj.type == '>')
		{
			Result->nodeInfo.ival = left.nodeInfo.ival > right.nodeInfo.ival;
		}
		else if (Ast->obj.type == '<')
		{
			Result->nodeInfo.ival = left.nodeInfo.ival < right.nodeInfo.ival;
		}
		else
		{
			EXCEPTION;
			printf("error parsing the Ast!\n");
			goto cleanup;
		}
	}
	else if ((left.type == right.type) && (right.type == LIST_TYPE))
	{
		if (Ast->obj.type == '+')
		{	
			DEBUG_MSG("left: %p\n", left.nodeInfo.lval);
			DEBUG_MSG("right: %p\n", right.nodeInfo.lval);
			Result->nodeInfo.lval = listDup(left.nodeInfo.lval, right.nodeInfo.lval);
		}
		else
		{
			EXCEPTION;
			printf("type: %s\n", getType(Ast->obj.type));
			goto cleanup;
		}
	}
	else if ((left.type == NUMB) && (right.type == LIST_TYPE))
	{
		Result->nodeInfo.ival = left.nodeInfo.ival + len(right.nodeInfo.lval);
	}
	else if ((left.type == LIST_TYPE) && (right.type == NUMB))
	{
		Result->nodeInfo.lval = cons_list(left.nodeInfo.lval, right.nodeInfo.ival);
	}
	else
	{
		DEBUG_MSG("left %p, right %p\n", left, right);
		DEBUG_MSG("left type %s\n", getType(left.type));	
		DEBUG_MSG("right type %s\n", getType(right.type));
		EXCEPTION;
		goto cleanup;
	}
	
	status = STATUS_SUCCESS;
cleanup:
	if (Result->type == NOT_ASSIGNED_TYPE)
	{
		if (left.type != NOT_ASSIGNED_TYPE)
		{
			Result->type = left.type;
		}
		DEBUG_MSG("res: %d, type: %d\n", Result->nodeInfo.ival, Result->type);
	}
	if (variableObjectReference)
	{
		memcpy(Result, variableObjectReference, sizeof(*Result));
		if (FieldReference)
		{
			*FieldReference = variableObjectReference;
		}
	}
	
	DEBUG_MSG("return: %d\n", status);
	return status;
}
