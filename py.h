#ifndef _PY_H_
#define _PY_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "y.tab.h"

//#define DEBUG_MSG printf
#define DEBUG_MSG
#define NOT_ASSIGNED_TYPE 0
#define MISSING_TYPE -1

typedef int STATUS_TYPE;

typedef union
{
	int ival;
	struct _LIST* lval;
}NODE_INFO, *PNODE_INFO;

typedef struct _OBJECT
{
	int type;
	NODE_INFO nodeInfo;
}OBJECT, *POBJECT;

typedef struct _LIST
{
	OBJECT value;
	struct _LIST *next;
}LIST, *PLIST;

typedef struct _NODE
{
	OBJECT obj;
	struct _NODE* left;
	struct _NODE* right;
} NODE, *PNODE;

PNODE cons_node(int type, NODE_INFO nodeInfo, PNODE left, PNODE right);

STATUS_TYPE
parseAst(
	PNODE Ast,
	POBJECT Result,
	POBJECT *FieldReference
	);


void
printList(
	PLIST List
	);
void
prettyPrintAst(
	PNODE Ast
	);
PLIST
cons_list(
	PLIST List,
	int Element
	);
PLIST
listDup(
	PLIST List1,
	PLIST List2
	);
PLIST
listSlice(
	PLIST List,
	int Index1,
	int Index2
	);
POBJECT
getListElement(
	PLIST List,
	int Index
	);
POBJECT
setListElement(
	PLIST *List,
	int Index,
	POBJECT Value
	);
	
void
printNode(
	POBJECT Obj,
	char* String
	);

void
freeAst(
	PNODE Node
	);
	
typedef struct
{
	int key;
	int used;
	OBJECT value;
} HASH_TABLE, *PHASH_TABLE;

#define HASH_SIZE 23
extern HASH_TABLE gHashTable[HASH_SIZE];
unsigned long hash_djb2(unsigned char *Str);
int quadratic_probing_insert(PHASH_TABLE Hashtable, int Key);
int quadratic_probing_search(PHASH_TABLE Hashtable, int Key);


#endif