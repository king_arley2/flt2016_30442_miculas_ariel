%{
#include "y.tab.h"
#include "py.h"
%}
%%
[0-9]+ {yylval.ival = atoi(yytext); return NUMB;}
[()+*\/\[\],=><;{}:-] return *yytext;
"//".* ;

">="            return GE;
"<="            return LE;
"=="            return EQ;
"!="            return NE;
"while"         return WHILE;
"if"            return IF;
"print"         return PRINT;


[_a-zA-Z][_a-zA-Z0-9]* {yylval.hash_index=hash_djb2((unsigned char*)yytext);return VARIABLE;}

[ \t\n] ;

%%
int yywrap(void)
{
    return 1;
}