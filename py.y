%{
#include <stdio.h>
#include "py.h"
extern FILE * yyin;
%}


%union 
{
	int ival;
	int hash_index;
	struct _NODE *node;
	struct _LIST *lval;
}
%token <ival> NUMB
%token <hash_index> VARIABLE
%token LIST_TYPE LIST_ELEMENT LIST_SLICE WHILE IF PRINT
%type <lval> enum
%type <node> expr var_form stmt stmt_list numb_form list_elem slice list_slice
%left GE LE EQ NE '>' '<'
%left '+' '-'
%left '*' '/'

%%

file: file stmt
		{
			OBJECT object = {0};
			prettyPrintAst($2);
			parseAst($2, &object, NULL);
			DEBUG_MSG("printing node...\n");
			printNode(&object, NULL);
			printf("\n");
			DEBUG_MSG("freeAst\n");
			freeAst($2);
		}
	|	/*empty*/
	;
	
stmt: expr
	| var_form '=' expr {$$ = cons_node('=', (NODE_INFO)0, $1, $3);}
	| list_elem '=' expr {$$ = cons_node('=', (NODE_INFO)0, $1, $3);}
	| IF '(' expr ')' stmt {$$ = cons_node(IF, (NODE_INFO)0, $3, $5);}
	| WHILE '(' expr ')' stmt {$$ = cons_node(WHILE, (NODE_INFO)0, $3, $5);}
	| PRINT expr {$$ = cons_node(PRINT, (NODE_INFO)0, $2, NULL);}
	| '{' stmt_list '}' { $$ = $2; }
    ;

stmt_list:
	  stmt
	| stmt_list stmt  { $$ = cons_node(';', (NODE_INFO)0, $1, $2); }
	; 
	
numb_form:
	NUMB {$$ = cons_node(NUMB, (NODE_INFO)$1, NULL, NULL);}

var_form:
	VARIABLE {$$ = cons_node(VARIABLE, (NODE_INFO)$1, NULL, NULL);}

list_elem:
	var_form '[' expr ']' {$$ = cons_node(LIST_ELEMENT, (NODE_INFO)0, $1, $3);}
	| list_elem '[' expr ']' {$$ = cons_node(LIST_ELEMENT, (NODE_INFO)0, $1, $3);}
	
slice:
	'[' NUMB ':' NUMB ']' 
	{
		PLIST list = cons_list(NULL, $4);
		list = cons_list(list, $2);
		$$ = cons_node(LIST_TYPE, (NODE_INFO)list, NULL, NULL);
	}
	| '[' ':' NUMB ']'
	{
		PLIST list = cons_list(NULL, $3);
		list = cons_list(list, 0);
		$$ = cons_node(LIST_TYPE, (NODE_INFO)list, NULL, NULL);
	}
	| '[' NUMB ':'  ']'
	{
		PLIST list = cons_list(NULL, 0x7FFFFFFF);
		list = cons_list(list, $2);
		$$ = cons_node(LIST_TYPE, (NODE_INFO)list, NULL, NULL);
	}
	| '[' ':' ']'
	{
		PLIST list = cons_list(NULL, 0x7FFFFFFF);
		list = cons_list(list, 0);
		$$ = cons_node(LIST_TYPE, (NODE_INFO)list, NULL, NULL);
	}
list_slice:
	var_form slice {$$ = cons_node(LIST_SLICE, (NODE_INFO)0, $1, $2);}
	| list_slice slice {$$ = cons_node(LIST_SLICE, (NODE_INFO)0, $1, $2);}
	
expr: expr '+' expr {$$ = cons_node('+', (NODE_INFO)0, $1, $3);}
		| expr '-' expr {$$ = cons_node('-', (NODE_INFO)0, $1, $3);}
		| expr '*' expr {$$ = cons_node('*', (NODE_INFO)0, $1, $3);}
		| expr '/' expr {$$ = cons_node('/', (NODE_INFO)0, $1, $3);}
		| expr '<' expr {$$ = cons_node('<', (NODE_INFO)0, $1, $3);}
        | expr '>' expr {$$ = cons_node('>', (NODE_INFO)0, $1, $3);}
        | expr GE expr  {$$ = cons_node(GE, (NODE_INFO)0, $1, $3);}
        | expr LE expr  {$$ = cons_node(LE, (NODE_INFO)0, $1, $3);}
        | expr NE expr  {$$ = cons_node(NE, (NODE_INFO)0, $1, $3);}
        | expr EQ expr  {$$ = cons_node(EQ, (NODE_INFO)0, $1, $3);}
		| numb_form
		| var_form
		| list_elem
		| list_slice
		| '(' expr ')' {$$ = $2;}
		| '[' enum ']' {$$ = cons_node(LIST_TYPE, (NODE_INFO)$2, NULL, NULL);}
		| '[' ']' {$$ = cons_node(LIST_TYPE, (NODE_INFO)(struct _LIST*)NULL, NULL, NULL);}
		;
	
	
enum: NUMB {$$ = cons_list(NULL, $1);}
	| NUMB ',' enum {$$ = cons_list($3, $1);}
	;


%%


int yyerror(char *s)
{
    fprintf(stderr, "%s\n", s);
	return 0;
}

int main(int argc, char **argv)
{
	if (argc > 1)
	{
		yyin = fopen(argv[1],"r");
	}
	if (argc > 2)
	{
		freopen( argv[2], "w", stdout);
	}
    yyparse();
    return 0;
}
