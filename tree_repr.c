#include "py.h"

#define CHAR_MATRIX_LINES	100
#define CHAR_MATRIX_COLUMNS 100
#define NODE_SIZE_IN_CHARS 4

char charMatrix[CHAR_MATRIX_LINES][CHAR_MATRIX_COLUMNS][NODE_SIZE_IN_CHARS];

void
sprintfCenter(
	char *Destination,
	char *String,
	int FormatSize
	)
{
	int len = 0;
	if (!String)
	{
		return;
	}
	len = strlen(String);
	snprintf(Destination, FormatSize, "%*s%*s",(FormatSize+len)/2,String,(FormatSize-len)/2,"");
	
}

void
printNode(
	POBJECT Obj,
	char* String
	)
{
	char stringRepr[NODE_SIZE_IN_CHARS];
	if (!Obj)
	{
		printf("<NULL>");
		return;
	}
	if (Obj->type == NUMB)
	{
		if (String)
		{
			snprintf(stringRepr, NODE_SIZE_IN_CHARS, "%d", Obj->nodeInfo.ival);
			sprintfCenter(String, stringRepr, NODE_SIZE_IN_CHARS);
		}
		else
		{
			printf("%d", Obj->nodeInfo.ival);
		}
	}
	else if (Obj->type == LIST_TYPE)
	{
		if (String)
		{
			sprintfCenter(String, "LIST", NODE_SIZE_IN_CHARS);
		}
		else
		{
			printList(Obj->nodeInfo.lval);
		}
	}
	else if (Obj->type > 0 && Obj->type < 128)
	{
		if (String)
		{
			snprintf(stringRepr, NODE_SIZE_IN_CHARS, "%c", Obj->type);
			sprintfCenter(String, stringRepr, NODE_SIZE_IN_CHARS);
		}
		else
		{
			printf("%c", Obj->type);
		}
	}
	else
	{
		char* repr = NULL;
		if (Obj->type == VARIABLE)
		{
			repr = "VAR";
		}
		else if (Obj->type == GE)
		{
			repr = ">=";
		}
		else if (Obj->type == LE)
		{
			repr = "<=";
		}
		else if (Obj->type == EQ)
		{
			repr = "==";
		}
		else if (Obj->type == NE)
		{
			repr = "!=";
		}
		else if (Obj->type == IF)
		{
			repr = "if";
		}
		else if (Obj->type == WHILE)
		{
			repr = "while";
		}
		else if (Obj->type == PRINT)
		{
			repr = "print";
		}
		else if (Obj->type == LIST_ELEMENT)
		{
			repr = "ELEM";
		}
		else if (Obj->type == MISSING_TYPE)
		{
			repr = "MISSING";
		}
		else if (Obj->type == NOT_ASSIGNED_TYPE)
		{
			repr = "UNDEF";
		}
		else if (Obj->type == LIST_SLICE)
		{
			repr = "SLICE";
		}
		else
		{
			repr = "UNK";
		}
		
		if (String)
		{
			sprintfCenter(String, repr, NODE_SIZE_IN_CHARS);
		}
		else
		{
			printf(repr);
		}
	}
}

int
getMaxLevel(
	PNODE Ast
	)
{
	int l1, l2;
	if (Ast == NULL)
	{
		return 0;
	}
	l1 = getMaxLevel(Ast->left);
	l2 = getMaxLevel(Ast->right);
	DEBUG_MSG("l1:%d, l2: %d\n", l1,l2);
	return l1 > l2 ? l1 + 1 : l2 + 1;
}

void
buildCharMatrix(
	PNODE Ast,
	int Level,
	int ReverseLevel,
	int ColIndex
	)
{
	int columnIndex;
	
	if (Ast)
	{
		columnIndex = ColIndex;
		DEBUG_MSG("mat index %d %d\n", Level, columnIndex);
		printNode(&Ast->obj, charMatrix[Level][columnIndex]);
		
		columnIndex = ColIndex - (1 << (ReverseLevel - 1));
		buildCharMatrix(Ast->left, Level + 1, ReverseLevel-1, columnIndex);
		
		columnIndex = ColIndex + (1 << (ReverseLevel - 1));
		buildCharMatrix(Ast->right, Level + 1, ReverseLevel-1, columnIndex);
	}
}

void
printCharMatrix(
	int NrLevels
	)
{
	int i = 0;
	int j = 0;

	for (i = 0; i < NrLevels; i++)
	{
		for (j = 0; j < (1 << NrLevels) - 1; j++)
		{
			if (charMatrix[i][j][0] == 0)
			{
				printf("%*s", NODE_SIZE_IN_CHARS, "");
			}
			else
			{
				printf(charMatrix[i][j]);	
			}
		}
		printf("\n");
	}
}

void
prettyPrintAst(
	PNODE Ast
	)
{
	int i = 0;
	int j = 0;
	int nrLevels = 0;
	int rootColIndex = 0;
	for (i = 0; i < CHAR_MATRIX_LINES; i++)
	{
		for (j = 0; j < CHAR_MATRIX_COLUMNS; j++)
		{
			charMatrix[i][j][0]=0;
		}
	}
	
	nrLevels = getMaxLevel(Ast);
	
	DEBUG_MSG("levels: %d\n", nrLevels);
	rootColIndex = (1 << (nrLevels - 1)) - 1;
	DEBUG_MSG("root col index: %d\n", rootColIndex);
	buildCharMatrix(Ast, 0, nrLevels - 1, rootColIndex);
	printCharMatrix(nrLevels);
	printf("\n");
}


void
inOrder(
	PNODE Ast
	)
{
    if(Ast)
	{
        inOrder(Ast->left);
		printNode(&Ast->obj, NULL);
        inOrder(Ast->right);
    }
}

void
preOrder(
	PNODE Ast
	)
{
    if(Ast)
	{
		printNode(&Ast->obj, NULL);
        preOrder(Ast->left);
        preOrder(Ast->right);
    }
}

void
postOrder(
	PNODE Ast
	)
{
    if(Ast)
	{
        postOrder(Ast->left);
        postOrder(Ast->right);
		printNode(&Ast->obj, NULL);
    }
}